const { workerData, parentPort } = require("worker_threads");

function sumiraj(niz) {
    return niz.map(x => x * x);
}

parentPort.postMessage(
    sumiraj(workerData.niz)
);