/*
 * Napisati program koji u dva odvojena worker thread-u kreira
 * nizove. Nakon kreiranja dva niza program prosledjuje nizove
 * novom worker thread-u koji računa sumu elemenata nizova i kao
 * rezultat vraća novi niz.
 */

const { Worker } = require("worker_threads");

const kreirajGenerator = function (brojElemenata) {
    return new Promise((resolve, reject) => {
        const generator = new Worker("./generator.js", { workerData: { brojElemenata } });
        generator.on("message", resolve);
        generator.on("error", reject);
    })
}

const kreirajSabirac = function (niz1, niz2) {
    return new Promise((resolve, reject) => {
        const generator = new Worker("./suma.js", { workerData: { niz1, niz2 } });
        generator.on("message", resolve);
        generator.on("error", reject);
    })
}
Promise.all([
    kreirajGenerator(50000),
    kreirajGenerator(50000)]).then(r => kreirajSabirac(...r), err => console.log(err))
    .then(r => console.log(r), err => console.log(err));